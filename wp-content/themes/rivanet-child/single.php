<?php

get_header();
the_post(); ?>
<div class="container-fluid blog-template">
        <?php get_template_part( 'partials/header-featured-image' ); ?>
        <?php get_template_part( 'partials/news/news-note-section' ); ?> 
        <?php get_template_part( 'partials/news/news-submenu' ); ?>
        <?php get_template_part( 'partials/news/news-single-post' ); ?>             
</div>

<?php get_footer(); ?>