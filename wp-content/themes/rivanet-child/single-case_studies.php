<?php 
/* 
Single Case Studies
*/
the_post();
get_header();  ?>
    <div class="container-fluid">
    
        <?php get_template_part( 'partials/header-featured-image' ); ?>
        <?php the_content(); ?>

        <section class="vc_section rw-section arrows">
            <div class="vc_row wpb_row vc_row-fluid rw-fullwidth">   
                <div class="previous-next">
                    <?php   
                    $post_id = $post->ID; // current post ID
                    
                    $args = array(
                        'post_type' => 'case_studies',
                        'orderby'  => 'post_date',
                        'order'    => 'DESC'
                    );
                    
                    $posts = get_posts( $args );

                    // get IDs of posts retrieved from get_posts
                    $ids = array();
                    foreach ( $posts as $thepost ) {
                        $ids[] = $thepost->ID;
                    }
                    // get and echo previous and next post in the same category
                    $thisindex = array_search( $post_id, $ids );

                    if ( ! empty( $ids[ $thisindex - 1 ] ) ) {
                        ?><a rel="prev" class="prev" href="<?php echo get_permalink($ids[ $thisindex - 1 ]) ?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Previous</a><?php
                    }
                    if ( ! empty( $ids[ $thisindex + 1 ]) ) {
                        ?><a rel="next" class="next" href="<?php echo get_permalink($ids[ $thisindex + 1 ]) ?>">Next <i class="fa fa-angle-double-right" aria-hidden="true"></i></a><?php
                    }?>
                </div>
                </div>
        </section>

    </div>

 <?php get_footer(); ?>