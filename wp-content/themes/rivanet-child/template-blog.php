<?php include 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
<?php

/*

Template Name: Blog Page

*/

get_header(); 
the_post();
?>
<div class="container-fluid blog-template">
        <?php
	get_template_part( 'partials/header-featured-image' ); 
        setup_postdata($post);
        ?>

	<?php get_template_part( 'partials/news/news-note-section' ); ?>
	<?php get_template_part( 'partials/news/news-submenu' ); ?>
	<?php get_template_part( 'partials/news/news-list' );?>        
	<?php 
	if ( is_home() || is_category() || is_single() ) {
		if (is_singular( 'post' ) ) {
			$page_for_posts = get_option( 'page_for_posts' );
		}
		else if (is_singular( 'case_studies' ) ) {
			$page_for_posts = null;
		}
		else {
			$page_for_posts = get_option( 'page_for_posts' );
		}
	}
	else {
		$page_for_posts = null;
	}

	if ( $page_for_posts ) {           
			// the_content() doesn't accept a post ID parameter
			
			if ( $post = get_post( $page_for_posts ) ) {
				setup_postdata( $post ); //  "posts" page is now current post for most template tags        
				the_content();
				wp_reset_postdata(); // So everything below functions as normal
			}
		}
	?>
</div>

<?php get_footer(); ?>