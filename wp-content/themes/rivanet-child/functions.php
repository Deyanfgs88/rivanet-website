<?php
    include_once 'includes/wp-setup.php';


// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - HOOK: AFTER SETUP THEME
    add_action( 'after_setup_theme' , function(){
    // add_image_size       => Documentation: https://developer.wordpress.org/reference/functions/add_image_size/
        global $image_sizes;
        $image_sizes = array(
            'rw-module-big' => array( 'width' => 1500 , 'height' => 653 , 'crop' => true )
        );

        foreach( $image_sizes as $size_id => $image_size ):
            add_image_size( 
                $size_id,
                $image_size[ 'width' ],
                $image_size[ 'height' ],
                $image_size[ 'crop' ]
            );
        endforeach;

    } ); // END after_setup_theme


/** Convert Hex to RGB **/

function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   //return implode(",", $rgb); // returns the rgb values separated by commas
   return $rgb; // returns an array with the rgb values
}

/*** Custom Login URL ***/
function custom_loginlogo_url($url) {
	return get_bloginfo('url');
}
add_filter( 'login_headerurl', 'custom_loginlogo_url' );



/*** Add Category Parameter like dropdown for shortcode Case Studies Posts and Posts Blog VC ***/

add_action( 'vc_after_init', 'add_parameter_case_studies' );

function add_parameter_case_studies(){

    $all_categories = get_categories();
    $cat_dropdown = array();
    foreach ($all_categories as $category){
        $cat_dropdown[] = $category->slug;
    }

    array_unshift($cat_dropdown, 'Select Category: ');

    $attributes = array(
        'type' => 'dropdown',
        'heading' => "Categories List",
        'param_name' => 'cat',
        'value' => $cat_dropdown,
        'description' => __( 'If you choose the default option "Select Category: " it will show all the posts', "my-text-domain" )
    );
    vc_add_param( 'case_studies_list', $attributes );
    vc_add_param( 'news_posts', $attributes );
}


/*** Custom Excerpt Length ***/

function custom_excerpt_length( $length ) {
    return 30;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );