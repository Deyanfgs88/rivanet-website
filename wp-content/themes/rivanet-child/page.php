<?php include 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
<?php get_header(); the_post();?>
    <div class="container-fluid">
    
        <?php get_template_part( 'partials/header-featured-image' ); ?>
        <?php the_content(); ?>
    </div>
<?php get_footer();