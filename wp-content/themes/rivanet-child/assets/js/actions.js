(function() {
    /* * * * * * * * * * * * * * * * Variables */


    /* * * * * * * * * * * * * * * * Functions */


    /* * * * * * * * * * * * * * * * Events  */

    jQuery(function($) {
        $(document).ready(function() {
            $(".navbar-toggle").on("click", function() {
                $(this).toggleClass("active");
            });

            /* Show more "Who We Are" Section */
            $(".btn-showmore.individual button").on("click", function() {
                $(this).parent(".btn-showmore").siblings(".description").slideToggle("slow");
                $(this).find("i").toggleClass("fa-angle-down fa-angle-up");
                var textElement = $(this).parents(".member-team").find(".btn-showmore-text").find("p");
                var text = textElement.text();
                textElement.text(
                    text == "Show more" ? "Show less" : "Show more");
            });

            $(".btn-showmore.group button").on("click", function() {
                $(this).parents(".rw-module").find(".member-team .description").slideToggle("slow");
                $(this).find("i").toggleClass("fa-angle-down fa-angle-up");

                var textElement = $(this).parents(".btn-row").find(".btn-showmore-text").find("p");
                var text = textElement.text();
                textElement.text(
                    text == "Show more" ? "Show less" : "Show more");
            });

            /* Submenu categories */
            $(".subnav-categories .navbar-toggle").on("click", function() {
                if (!(Array.from($(".subnav-categories .collapse .cat-item.current-cat")).length)) {
                    $(this).parents(".navbar").find(".collapse .cat-item:not(:first-of-type)").slideToggle("slow");
                } else {
                    $(this).parents(".navbar").find(".collapse li").slideToggle("slow");
                }
                $(this).toggleClass("fa-angle-double-down fa-angle-double-up");
            });

            if (!(Array.from($(".subnav-categories .collapse .cat-item.current-cat")).length)) {
                $(".subnav-categories .collapse .cat-item:first-of-type").show();
            }

            /* All span on main Contact Form withthe same width */
            var greatestWidth = 0;   // Stores the greatest width
            var formSelector = "#custom-form .interest .wpcf7-list-item";
            $(formSelector).each(function() {    // Select the elements you're comparing
            
                var theWidth = $(this).width();   // Grab the current width
            
                if( theWidth > greatestWidth) {   // If theWidth > the greatestWidth so far,
                    greatestWidth = theWidth;     //    set greatestWidth to theWidth
                }
            });
            $(formSelector).width(greatestWidth + 10);     // Update the elements you were comparing


            /* Quote and cite fontz size 
            console.log($("span.quote").text().length);
            if ($("span.quote").text().length > 100) {
                console.log(this);
                $("span.quote").css("font-size", "40px");
                $("span.cite").css("font-size", "24px");
            }*/
            
        });
    });

}).call(this);

jQuery(document).ready(function() {
    // Add Background color when scroll
    // Change color contact header when scroll
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 10) {
            jQuery(".navbar").addClass("bg-color-blue");
            jQuery(".call_us_section").addClass("contact-header-color-green");
            jQuery(".call_us_section .telephone").addClass("contact-header-color-green");
        } else {
            jQuery(".navbar").removeClass("bg-color-blue");
            jQuery(".call_us_section").removeClass("contact-header-color-green");
            jQuery(".call_us_section .telephone").removeClass("contact-header-color-green");
        }
    });

    // Appears submenu item menu services support on click
    jQuery(".menu-item").click(function() {
        if (jQuery("#" + this.id + " .sub-menu").hasClass("display-flex")) {
            jQuery("#" + this.id + " .sub-menu").removeClass("display-flex");
        } else {
            jQuery(".sub-menu").removeClass("display-flex");
            jQuery("#" + this.id + " .sub-menu").addClass("display-flex");
        }
    });

});