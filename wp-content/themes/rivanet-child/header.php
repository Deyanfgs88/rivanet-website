<?php include_once 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head( ); ?>
</head>
<body <?php body_class( RW_ENVIRONMENT ); ?> >
<div id="page-wrap">
	<header>
	<div class="container-header">
			<nav class="navbar navbar-default" role="navigation"> 
				<div class="navbar-wrap">
					<div class="navbar-header"> 
						<?php ses_custom_logo();?>
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse"> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span> 
							<span class="icon-bar"></span> 
						</button> 
					</div> 
					<!-- Collect the nav links, forms, and other content for toggling --> 
					<div id="main-menu-wrap" class="collapse navbar-collapse navbar-ex1-collapse custom-menu"> 
						<div class="call_us_section hidden-xs">
							<div class="content">
								<span class="caption">
									<i class="fa fa-phone" aria-hidden="true"></i>
									Call us today </span>
								<?php
									$phone = get_theme_mod('telephone');
									$phone_removed_whitespace = preg_replace('/\s+/', '', $phone);
								?>
								<a href="tel:+<?php echo $phone_removed_whitespace; ?>" class="telephone"><?php echo $phone; ?></a>
							</div>
						</div>
						<?php /* Primary navigation */
							if ( has_nav_menu( 'primary' ) ):
								wp_nav_menu( array( 'theme_location' => 'primary' ));
								//,'menu_class' => 'nav','walker' => new wp_bootstrap_navwalker()) );
							endif;
						?>
					</div>
				</div>
			</nav>
		</div>
	</header>
	<div id="content-wrap">