<section class="vc_section rw-section post-list">
    <div class="vc_row wpb_row vc_row-fluid rw-fullwidth">
    <?php
        if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); }
        elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
        else { $paged = 1; }

        $query_args = array(
            'post_type'         => 'post',
            'paged'             => $paged,
            'page'              => $paged,
            'cat'               => ''
        );

        if (get_queried_object_id() != 299) {
            $query_args['cat'] = get_queried_object_id();
        }

        $the_query = new WP_Query( $query_args ); ?>

        <?php if ( $the_query->have_posts() ) : ?>

        <!-- the loop -->
        <?php 
            while ( $the_query->have_posts() ) : $the_query->the_post();?>
            <article class="col-md-6">
                <?php 
                $categories = get_the_category();
                if ( ! empty( $categories ) ): ?>
                    <div class="cat-item cat-item-<?php echo esc_html( $categories[0]->term_id ); ?> cat-<?php echo esc_html( $categories[0]->slug ); ?>">
                        <span><?php echo esc_html( $categories[0]->name ); ?></span>
                    </div>
                <?php endif ?>
                <h3><?php the_title(); ?></h3>
                <div class="text">
                    <?php the_excerpt(); ?>
                </div>
                <div class="read-more">
                    <a href="<?php echo get_permalink( $post->ID ); ?>">Read more
                    <i class="vc_btn3-icon fa fa-long-arrow-right"></i></a>
                </div>
            </article>
        <?php endwhile;?>
        
        <nav class='custom-pagination col-sm-12'>
            <?php 
                echo paginate_links( array(
                    'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
                    'total'        => $the_query->max_num_pages,
                    'current'      => max( 1, get_query_var( 'paged' ) ),
                    'format'       => '?paged=%#%',
                    'show_all'     => false,
                    'type'         => 'plain',
                    'end_size'     => 2,
                    'mid_size'     => 1,
                    'prev_next'    => true,
                    'prev_text'    => sprintf( '<i></i> %1$s', __( '«', 'text-domain' ) ),
                    'next_text'    => sprintf( '%1$s <i></i>', __( '»', 'text-domain' ) ),
                    'add_args'     => false,
                    'add_fragment' => '',
                ) );
            ?>
        </nav>

        <?php endif; ?>
    
    </div>
</section>