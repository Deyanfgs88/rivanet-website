<section class="vc_section rw-section subnav-categories">
    <div class="vc_row wpb_row vc_row-fluid rw-fullwidth">        
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header pull-left">
                    <span class="navbar-brand">Categories:</span>
                </div>
                <div class="navbar-header pull-right">
                    <ul class="nav navbar-nav pull-left">
                        <li class="cat-item cat-item-all <?php if (get_queried_object_id() == 299) echo 'current-cat' ?>"><a href="<?php echo get_home_url(); ?>/news">VIEW ALL</a></li>
                    </ul>
                </div>

                <div class="collapse navbar-collapse in" id="myNavCat">
                    <ul class="nav navbar-nav">
                        <?php 
                            $args = array(
                                'show_count'         => 0,
                                'use_desc_for_title' => 0,
                                'title_li'           => 0                        
                            );
                            $categories_menu = get_categories($args);
                            
                            foreach ($categories_menu as $category_menu){
                                echo '<li class="cat-item cat-item-' . $category_menu->term_id . ' cat-' . $category_menu->slug . '"><a href="' . get_home_url() . '/category/' . $category_menu->slug . '">' . $category_menu->cat_name . '</a></li>';
                            }
                        ?>
                    </ul>
                    <span><i class="fa fa-angle-double-down navbar-toggle" aria-hidden="true"></i></span>
                </div>
            </div>
        </nav>
    </div>
</section>