<?php

$the_blog_slug = 'news';
$args = array(
  'name'        => $the_blog_slug,
  'post_type'   => 'page',
  'post_status' => 'publish',
  'numberposts' => 1
);
$my_blog_post = get_posts($args);

?>

<div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-1 vc_col-md-10">
    <div class="vc_column-inner">
        <div class="wpb_wrapper">
            <h2 class="vc_custom_heading title"><?php echo get_field('blog_introduction_title', $my_blog_post[0]->ID); ?></h2>
            <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text">
                <span class="vc_sep_holder vc_sep_holder_l">
                    <span class="vc_sep_line"></span>
                </span>
            </div>
            <div class="wpb_text_column wpb_content_element  sentence">
                <div class="wpb_wrapper">
                    <?php echo get_field('blog_caption', $my_blog_post[0]->ID); ?>
                </div>
            </div>
        </div>
    </div>
</div>