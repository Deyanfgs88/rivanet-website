<section class="vc_section rw-section post-content">
    <div class="vc_row wpb_row vc_row-fluid rw-fullwidth">        
        <h1><?php the_title(); ?></h1>
            <?php       
                if(get_field('subtitle')) {
                    echo '<h2 class="subtitle">';
                    the_field('subtitle');
                    echo '</h2>';
                }
            ?>
    </div>
</section>

<?php  the_content(); ?>

<section class="vc_section rw-section arrows">
    <div class="vc_row wpb_row vc_row-fluid rw-fullwidth">   
        <div class="previous-next">
            <?php   
            $post_id = $post->ID; // current post ID

            $args = array(
                'orderby'  => 'post_date',
                'order'    => 'DESC'
            );
            
            $posts = get_posts( $args );
            // get IDs of posts retrieved from get_posts
            $ids = array();
            foreach ( $posts as $thepost ) {
                $ids[] = $thepost->ID;
            }
            // get and echo previous and next post in the same category
            $thisindex = array_search( $post_id, $ids );

            if ( ! empty( $ids[ $thisindex - 1 ] ) ) {
                ?><a rel="prev" class="prev" href="<?php echo get_permalink($ids[ $thisindex - 1 ]) ?>"><i class="fa fa-angle-double-left" aria-hidden="true"></i> Previous</a><?php
            }
            if ( ! empty( $ids[ $thisindex + 1 ]) ) {
                ?><a rel="next" class="next" href="<?php echo get_permalink($ids[ $thisindex + 1 ]) ?>">Next <i class="fa fa-angle-double-right" aria-hidden="true"></i></a><?php
            }?>
        </div>
        </div>
</section>

