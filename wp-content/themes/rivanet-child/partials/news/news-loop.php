<div class="column_post">
    <div class="title_1">
        <?php $categories= get_the_category(); ?>
        <?php 
        $cat_names_array = array();
        foreach ($categories as $cat){
            array_push($cat_names_array, $cat->name);
        }
        $categories_names = implode(' & ', $cat_names_array);
        echo $categories_names;
        ?>
    </div>
    <div class="title_2">
        <?php echo the_title(); ?>
    </div>
    <div class="text_content">
        <?php echo the_excerpt(); ?>
    </div>
    <div class="link_to_read_more btn-nostyle">
        
        <?php echo '<a href="' . get_permalink($post->ID) . '">Read more<i class="fa fa-long-arrow-right"></i></a>' ;?>
    </div>
</div>