<section class="vc_section rw-section note-section">
    <div class="vc_row wpb_row vc_row-fluid note-section-content">
        <?php
            if (!is_single()){
                get_template_part( 'partials/news/news-introduction' ); 
            }
        ?>
    </div>
    <div class="vc_row wpb_row vc_row-fluid white-space">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner ">
                <div class="wpb_wrapper">
                    <div class="vc_empty_space" style="height: 0px">
                        <span class="vc_empty_space_inner"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>