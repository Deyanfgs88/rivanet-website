        <section class='vc_section rw-section featured-header'
            style='background-image:url("<?php 
            
            if ( is_home() || is_category() || is_single() ) {
                if (is_singular( 'post' ) ) {
                    $page_for_posts = get_option( 'page_for_posts' );
                }
                else if (is_singular( 'case_studies' ) ) {
                    $page_for_posts = null;
                }
                else {
                    $page_for_posts = get_option( 'page_for_posts' );
                }
            }
            else {
                $page_for_posts = null;
            }

   
            echo get_the_post_thumbnail_url($post->ID,'rw-module-big'); 
            ?>")'
        >
            <?php
                $hex_color = get_field('header_color', $post->ID);
                $rgb_color = hex2rgb($hex_color);
                $opacity = 0.7;
            ?>
            <?php
            if  (!is_single()){ ?>
                <div class="featured-box featured-box-full" style="background-color: rgba(<?php echo $rgb_color[0]; ?>, <?php echo $rgb_color[1]; ?>, <?php echo $rgb_color[2]; ?>, <?php echo $opacity; ?>);">
            <?php } else if (is_singular('case_studies')){ ?>
                <div class="featured-box featured-box-full" style="background-color: rgba(<?php echo $rgb_color[0]; ?>, <?php echo $rgb_color[1]; ?>, <?php echo $rgb_color[2]; ?>, <?php echo $opacity; ?>);">
            <?php } else{ ?>
                 <div class="featured-box featured-box-full" style="background-color: rgba(4, 103, 121, 0.7);">
            <?php
            }
            ?>
                <div class="rw-module header-title">
                    <?php
                        if (!is_single()){ ?>
                            <span class="quote"><?php echo get_field( 'quote', $post->ID ); ?></span>
                        <?php }else if (is_singular('case_studies')){ ?>
                            <span class="quote"><?php echo get_field( 'quote', $post->ID ); ?></span>
                        <?php } else{ ?>
                            <span class="quote">Latest News & Insight</span>
                        <?php
                        }
                    ?>
                    <?php
                        if (!is_single()){ ?>
                        <span class="cite"><?php echo get_field( 'cite', $post->ID ); ?></span>
                        <?php }else if (is_singular('case_studies')){ ?>
                            <span class="cite"><?php echo get_field( 'cite', $post->ID ); ?></span>
                        <?php } else{ ?>
                            <span class="cite"></span>
                        <?php
                        }
                    ?>
                </div>
            </div>
        </section>