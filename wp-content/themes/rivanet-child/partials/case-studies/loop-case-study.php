<?php 
global $count;
global $maxcount;
$colours = ['#004460','#046779','#5B6770'];
$count = $count +1;
if($count < ($maxcount +1)):
?>
        <div class="vc_row wpb_row vc_inner vc_row-fluid rw-module two-col tw-col-boxs vc_row-o-equal-height vc_row-flex">
            <div class="content-box-wrap with-content-box wpb_column vc_column_container vc_col-sm-12 vc_col-md-6" id="image-box" <?php
                if($count % 2 == 0 ) {?> style="order:2"
                <?php }?>>
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_left">
                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                    <?php the_post_thumbnail('full', ['class' => 'vc_single_image-img attachment-full']) ?>
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-box-wrap with-content-box wpb_column vc_column_container vc_col-sm-12 vc_col-md-6 vc_col-has-fill" id="content-box">
                <?php
                    if ($count >= count($colours)+1){
                        $rest_count = $count % count($colours);
                    }else{
                        $rest_count = $count;
                    }
                ?>
                <div class="vc_column-inner" style="background-color:<?php echo $colours[$rest_count-1]?>">
                    <div class="wpb_wrapper">
                        <h3 style="text-align:left" class="vc_custom_heading">
                            <?php the_title(); ?>
                        </h3>
                        <div class="wpb_text_column wpb_content_element equal-height">
                            <div class="wpb_wrapper">
                                    <?php the_excerpt(); ?>
                            </div>
                        </div>
                        <div class="vc_btn3-container  btn-nostyle vc_btn3-left">
                            <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom vc_btn3-icon-right vc_btn3-color-grey"
                                href="<?php the_permalink(); ?>">
                                View case study 
                                <i class="vc_btn3-icon fa fa-long-arrow-right"></i>
                                </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php endif;?>
