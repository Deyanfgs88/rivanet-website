<?php include_once 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
	</div> <?php // END content-wrap ?>
    <footer>
        <div class="container-general-footer">
            <div class="first-section-footer">
                <div class="social-networks-links-pillars">
                    <div class="social-networks">
                        <?php
                            // $link_social_twitter = get_theme_mod('twitter');
                            $link_social_linkedin = get_theme_mod('linkedin');
                            /*if (empty($link_social_twitter)){
                                $link_social_twitter = 'http://twitter.com';
                            }*/
                            if (empty($link_social_linkedin)){
                                $link_social_linkedin = 'http://linkedin.com';
                            }
                        ?>
                        <!-- <a href="<?php //echo $link_social_twitter; ?>" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a> -->
                        <a href="<?php echo $link_social_linkedin; ?>" target="_blank"><i class="fa fa-linkedin-square" aria-hidden="true"></i></a>
                        <a class="email-icon" href="mailto:<?php echo get_theme_mod('email'); ?>"><img src="<?php echo THEME_IMAGES; ?>email-icon.png" title="email icon" alt="email icon"></a>
                    </div>
                    <div class="links-pillars">
                        <a href="<?php echo get_permalink(get_theme_mod('technology_page'));?>" class="technology">Technology & Infrastructure</a>
                        <a href="<?php echo get_permalink(get_theme_mod('services_page'));?>" class="support">IT Support Services</a>
                        <a href="<?php echo get_permalink(get_theme_mod('security_page'));?>" class="security">Cyber Security</a>
                    </div>
                </div> <?php // .social-networks-links-pillars ?>
            </div> <?php // .first-section-footer ?>
            <div class="second-section-footer">
                <div class="logo">
                    <a href="<?php echo get_home_url();?>">
                        <img class="logo-footer" src="<?php echo get_theme_mod( 'secundary_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                    </a>
                </div>
                <div class="container-phone-email-address">
                    <div class="phone-email">
                        <div class="phone">
                            <p>Telephone:</p>
                            <?php
                                $phone = get_theme_mod('telephone');
                                $phone_removed_whitespace = preg_replace('/\s+/', '', $phone);
                            ?>
                            <a href="tel:+<?php echo $phone_removed_whitespace; ?>"><?php echo $phone; ?></a>
                        </div>
                        <div class="email">
                            <p>Email:</p>
                            <a href="mailto:<?php echo get_theme_mod('email'); ?>"><?php echo get_theme_mod('email'); ?></a>
                        </div>
                    </div> <?php // .phone-email ?>
                    <div class="address">
                        <p><?php echo get_theme_mod('address'); ?></p>
                    </div> <?php // .address ?>
                </div>
            </div> <?php // .second-section-footer ?>
            <div class="third-section-footer">
                <div class="copyright-links-policy">
                    <div class="copyright">
                        <p>&copy; 2017 RivaNET Limited. All rights reserved.</p>
                    </div>
                    <div class="links-policy">
                        <a href="<?php echo get_permalink(get_theme_mod('privacy_page'));?>">Privacy</a>
                        <a href="<?php echo get_permalink(get_theme_mod('terms_use_page'));?>">Terms of use</a>
                        <a href="<?php echo get_permalink(get_theme_mod('cookie_page'));?>">Cookie preferences</a>
                    </div>
                </div> <?php // .copyright-links-policy ?>
            </div>  
        </div> <?php // .third-section-footer ?>
    </footer>
</div> <?php // END page-wrap ?>
<?php wp_footer(); ?> 
</body>
</html>