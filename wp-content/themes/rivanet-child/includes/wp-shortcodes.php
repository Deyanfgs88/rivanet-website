<?php
//Case Studies List
function case_studies_list($atts) {
    $args = shortcode_atts(array(
        'num' => 'num',
        'cat' => 'cat'
    ), $atts);
   
    ob_start();
    global $maxcount, $case_studies_categ;
    $maxcount = esc_attr($args['num']);
    $case_studies_categ = esc_attr($args['cat']);

    if ($case_studies_categ == 'cat'){ // show all posts
        query_posts(array (
            'post_type' => 'case_studies',
            'orderby'   => 'date',
            'posts_per_page'  =>  $maxcount,
            'order'     => 'DESC'
        ));
    }else{
        query_posts(array (
            'post_type' => 'case_studies',
            'orderby'   => 'date',
            'posts_per_page'  =>  $maxcount,
            'order'     => 'DESC',
            'category_name' => $case_studies_categ
        ));                               
    }


    while(have_posts()) : the_post(); 
        get_template_part('partials/case-studies/loop-case-study');
    endwhile;
    wp_reset_query();
    return ob_get_clean();
}
add_shortcode('case_studies_list','case_studies_list');


//News posts List
function news_list($atts) {
    $args = shortcode_atts(array(
        'num' => 'num',
        'cat' => 'cat'
    ), $atts);

    ob_start();
    global $maxcount_posts, $posts_categ;
    $maxcount_posts = esc_attr($args['num']);
    $posts_categ = esc_attr($args['cat']);

    if ($posts_categ == 'cat'){ // show all posts
        query_posts(array (
            'post_type' => 'post',
            'orderby'   => 'date',
            'posts_per_page'  =>  $maxcount_posts,
            'order'     => 'DESC'
        ));
    }else{
        query_posts(array (
            'post_type' => 'post',
            'orderby'   => 'date',
            'posts_per_page'  =>  $maxcount_posts,
            'order'     => 'DESC',
            'category_name' => $posts_categ
        ));                               
    }
    
    ?>
    <div class="news-row">
        <?php
            while(have_posts()) : the_post(); 
                get_template_part('partials/news/news-loop');
            endwhile;
        ?>
    </div>
    <?php 
    wp_reset_query();
    return ob_get_clean();
}
add_shortcode('news_posts','news_list');