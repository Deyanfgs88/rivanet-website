<?php include_once 'ysnp.php';

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE CSS
    add_action( 'wp_enqueue_scripts' , function(){

        wp_register_style( // - - - - - - - - - - - - - - - - Theme Style
            'theme-style',
            THEME_CSS . 'theme-style.css',
            array( ),
            '16.11.01'
        );

        wp_enqueue_style( 'theme-style' );

    }, 20 ); 

    add_action ( 'login_enqueue_scripts' , function(){
        wp_register_style( // - - - - - - - - - - - - - - - - Login Style
            'admin-style',
            THEME_CSS . 'admin-style.css',
            array( ),
            '16.11.01'
        );

        wp_enqueue_style( 'admin-style' );

    }, 200);

/*

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ENQUEUE JS
    add_action( 'wp_enqueue_scripts' , function(){
        $deps = array('jquery'); 

        if('v4-alpha'== BOOTSTRAP_VERSION){ //--------------------------------Boostrap v4-alpha control
            $deps = array(THEME_SHORT_NAME . '-tether', THEME_SHORT_NAME . '-jqueryslim');
            wp_register_script( // - - - - - - - - - - - - - - - Tether (Boostrap v4)
                THEME_SHORT_NAME . '-tether',
                THEME_TOOLS . 'bootstrap/' . BOOTSTRAP_VERSION . '/js/tether.min.js',
                array( THEME_SHORT_NAME . '-jqueryslim' ),
                '16.11.01' 
            );

            wp_register_script( // - - - - - - - - - - - - - - - jQuery slim (Boostrap v4)
                THEME_SHORT_NAME . '-jqueryslim',
                THEME_TOOLS . 'bootstrap/' . BOOTSTRAP_VERSION . '/js/jquery-3.1.1.slim.min.js',
                array(),
                '16.11.01' 
            );
        }
            wp_register_script( // - - - - - - - - - - - - - - - Bootstrap
                THEME_SHORT_NAME . '-bootstrap',
                THEME_TOOLS . 'bootstrap/' . BOOTSTRAP_VERSION . '/js/bootstrap.min.js',
                $deps,
                BOOTSTRAP_VERSION
        );


        $default_deps = array( 'jquery' );
        if( true === CNF_USE_AJAX ):
            wp_register_script( // - - - - - - - - - - - - - - - Ajax events
                THEME_SHORT_NAME . '-ajax-events',
                THEME_JS .'ajax-events.js',
                array( 'jquery' ),
                '17.01.19' 
            );
            wp_localize_script( THEME_SHORT_NAME . '-ajax-events', 'ajax', array(
                'url'   => admin_url( 'admin-ajax.php' ) ,
                'auth'  => wp_create_nonce( THEME_LONG_NAME )
            ));
            array_push( $default_deps , THEME_SHORT_NAME . '-ajax-events' );
        endif;

        wp_register_script( // - - - - - - - - - - - - - - - Actions
            THEME_SHORT_NAME . '-actions',
            THEME_JS .'actions.js',
            $default_deps,
            '16.11.01' 
        );

        wp_enqueue_script( THEME_SHORT_NAME . '-bootstrap' );
        wp_enqueue_script( THEME_SHORT_NAME . '-actions' );

    }, 21 ); // END enqueue Javascript

    */