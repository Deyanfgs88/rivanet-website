<?php include 'ysnp.php';
function ses_customize_register($wp_customize) {
    //------------------------------------Sections
    $wp_customize->add_section('footer_section',array(
        'title'     => __('Footer',THEME_LONG_NAME),
        'priority'  => 30,
    ));
    $wp_customize->add_section('social_icons',array(
        'title'     => __('Social Networks',THEME_LONG_NAME),
        'priority'  => 30,
    ));
   //------------------------------------Settings
    //-------------------------------Address
        $wp_customize->add_setting('telephone')->transport = 'postMessage';
        $wp_customize->add_setting('email')->transport = 'postMessage';
        $wp_customize->add_setting('address')->transport = 'postMessage';

    //--------------------------------Social Network
        // $wp_customize->add_setting('twitter')->transport = 'postMessage';
        $wp_customize->add_setting('linkedin')->transport = 'postMessage';

    //--------------------------------Pillars
        $wp_customize->add_setting('technology_page')->transport = 'postMessage';
        $wp_customize->add_setting('services_page')->transport = 'postMessage';
        $wp_customize->add_setting('security_page')->transport = 'postMessage';

    //--------------------------------Policy
        $wp_customize->add_setting('privacy_page')->transport = 'postMessage';
        $wp_customize->add_setting('terms_use_page')->transport = 'postMessage';
        $wp_customize->add_setting('cookie_page')->transport = 'postMessage';

    
   //------------------------------------Controls
    //-------------------------------Address
        $wp_customize->add_control(
            'information_telephone',
            array(
                'label'     =>__('Telephone',THEME_LONG_NAME),
                'settings'  => 'telephone',
                'priority'  => 10,
                'section'   => 'footer_section'
        ));
        $wp_customize->add_control(
            'information_email',
            array(
                'label'     =>__('Email',THEME_LONG_NAME),
                'settings'  => 'email',
                'priority'  => 10,
                'section'   => 'footer_section'
        ));
        $wp_customize->add_control(
            'information_address',
            array(
                'label'     =>__('Address',THEME_LONG_NAME),
                'settings'  => 'address',
                'priority'  => 10,
                'section'   => 'footer_section'
        ));
    //--------------------------------Social Network
        /* $wp_customize->add_control(
            'social_twitter',
        array(
            'label'     =>__('Twitter',THEME_LONG_NAME),
            'settings'  => 'twitter',
            'priority'  => 10,
            'section'   => 'social_icons'
        )); */

        $wp_customize->add_control(
            'social_linkedin',
            array(
                'label'     =>__('Linkedin',THEME_LONG_NAME),
                'settings'  => 'linkedin',
                'priority'  => 10,
                'section'   => 'social_icons'
        ));

    //-------------------------------- Pillars Footer

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'techonology_page_control',
                array(
                    'label'     =>__('Select Technology & Infrastructure Page',THEME_LONG_NAME),
                    'settings'  => 'technology_page',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'services_page_control',
                array(
                    'label'     =>__('Select Support Services Page',THEME_LONG_NAME),
                    'settings'  => 'services_page',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'security_page_control',
                array(
                    'label'     =>__('Select Cyber Security Page',THEME_LONG_NAME),
                    'settings'  => 'security_page',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );

    //-------------------------------- Policy Footer

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'privacy_page_control',
                array(
                    'label'     =>__('Select Privacy Page',THEME_LONG_NAME),
                    'settings'  => 'privacy_page',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'terms_use_page_control',
                array(
                    'label'     =>__('Select Terms of use Page',THEME_LONG_NAME),
                    'settings'  => 'terms_use_page',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );

        $wp_customize->add_control(
            new WP_Customize_DropdownPages_Control(
                $wp_customize,
                'cookie_page_control',
                array(
                    'label'     =>__('Select Cookie Page',THEME_LONG_NAME),
                    'settings'  => 'cookie_page',
                    'priority'  => 10,
                    'section'   => 'footer_section',
                )
            )
        );

}

add_action( 'customize_register', 'ses_customize_register' );


if (class_exists('WP_Customize_Control')) {
    class WP_Customize_DropdownPages_Control extends WP_Customize_Control {
         public function render_content() { ?>
            <h3> <?php echo $this->label; ?> </h3>
            <select  <?php $this->link(); ?>>
            <?php
                query_posts(array( 
                'post_type' => 'page',
                'post_status' => 'publish',
                'orderby' => 'date', 
                'order' => 'ASC',
                'posts_per_page' => 20
            ) );  
                while (have_posts()) : the_post(); ?>
                <option value = "<?php echo get_the_ID(); ?>"><?php echo get_the_title();?></option>       
                <?php endwhile ?>
            </select>
       <?php  }
    }
}