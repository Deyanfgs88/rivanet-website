/* * * * * * * * * * * * * * * * Global Functions */
function ajax_request(action, data, method) {
    // because safari...
    if (!method) method = 'post';

    jQuery.ajax({
        type: method,
        dataType: "json",
        url: ajax.url,
        data: {
            action: action,
            query: data,
            nonce: ajax.auth,
            submit: "ajax"
        },
        success: function(data) {
            return data;
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    })
}
