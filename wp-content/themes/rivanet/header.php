<?php include_once 'includes/ysnp.php'; // this path needs to be added manually for each file ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<?php wp_head( ); ?>
</head>
<body <?php body_class( ); ?> >
<div id="page-wrap">
	<header>
        <div class="header-wrap logo-menu">
            <?php include_once 'partials/headers/logo-menu.php'; ?>
        </div>
	</header>
	<div id="content-wrap">