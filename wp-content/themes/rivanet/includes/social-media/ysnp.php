<?php // You shall not pass
if ( ! function_exists( 'add_action' ) ) {
    echo '<div style="border: 4px solid #888888; max-width: 500px; margin: 50px auto; background-color: #444444; text-align: center; padding: 10px 20px">';   
    echo '<p style="color: #f1f1ed; font-size: 16px; font-family: verdana">Hi there!  I\'m sorry, not much I can do when called directly.</p>';
    echo '</div>';
    exit;
};