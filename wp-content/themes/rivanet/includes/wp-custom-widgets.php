<?php include 'ysnp.php';

// Creating the widget 
class wpb_widget extends WP_Widget {

    private $structure;

    function __construct() {
        parent::__construct(
            // Base ID of your widget
            'wpb_widget', 

            // Widget name will appear in UI
            __( THEME_LONG_NAME . ' Social Links ', THEME_LONG_NAME ), 

            // Widget description
            array( 'description' => __( 'Social Media Links & contact info', THEME_LONG_NAME ), ) 
        );

        $this->structure = array(
            'title'      => array( 'type' => 'text' , 'text' => 'Title'         ),
            'phone'      => array( 'type' => 'text' , 'text' => 'Phone number'  ),
            'envelope'   => array( 'type' => 'text' , 'text' => 'Email'         ),
            'map-marker' => array( 'type' => 'text' , 'text' => 'Address'       ),
            'facebook'   => array( 'type' => 'link' , 'text' => 'Facebook URL'  ),
            'instagram'  => array( 'type' => 'link' , 'text' => 'Instagram URL' ),
            'linkedin'   => array( 'type' => 'link' , 'text' => 'Linkedin URL'  )
        );
    }

    // Creating widget front-end
    // This is where the action happens
    public function widget( $args, $instance ) {
        $title = apply_filters( 
            'widget_title', 
            $instance['title']
        );
        // before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if ( ! empty( $title ) )
            echo $args['before_title'] . $title . $args['after_title'];

        // This is where you run the code and display the output
        ob_start();
        echo $args['before_widget'];
        ?>
        <div class="contact-line">
            <i class="fa fa-phone" aria-hidden="true"></i>
            <a href="tel:<?php echo $instance['phone'];?>"><?php echo $instance['phone'];?></a>
        </div>

        <div class="contact-line contact-mail">
            <i class="fa fa-envelope" aria-hidden="true"></i>
            <a href="mailto:<?php echo $instance['envelope'];?>"><?php echo $instance['envelope'];?></a>
        </div>

        <div class="contact-line">
            <i class="fa fa-map-marker" aria-hidden="true"></i>
            <span><?php echo $instance['map-marker'];?></span>
        </div>
        
        <div class="<?php echo THEME_LONG_NAME;?>-contact-line socials">
        <?php foreach( array( 'facebook' , 'instagram' , 'linkedin' ) as $social_link ): ?>
            <a href="<?php echo $instance[$social_link];?>"  target="_blank"><i class="fa fa-<?php echo $social_link;?>"  aria-hidden="true"></i></a>
        <?php endforeach; ?>
        </div>
        <?php
        echo $args['after_widget'];
        echo ob_get_clean();
    }
            
    // Widget Backend 
    public function form( $instance ) {
        if ( ! isset( $instance[ 'title' ] ) ) $instance[ 'title' ] = __( 'Social Links', THEME_LONG_NAME );
        foreach( $this->structure as $id => $text ):
            $value = ( isset( $instance[ $id ] ) ? $instance[ $id ] : null );
            ?>
            <p>
                <label for = '<?php echo $this->get_field_id( $id ); ?>'><?php _e( $text[ 'text' ] ); ?></label> 
                <input  id = '<?php echo $this->get_field_id( $id ); ?>'
                    class = 'widefat'
                        name = '<?php echo $this->get_field_name( $id ); ?>'
                        type = 'text'
                    value = '<?php echo esc_attr( $value ); ?>' 
                />
            </p>
            <?php 
        endforeach; 
    }
      
    // Updating widget replacing old instances with new
    public function update( $new_instance, $old_instance ) {
        $instance = array();
        foreach( $this->structure as $id => $text ):
            $instance[ $id ] = ( ! empty( $new_instance[ $id ] ) ) ? strip_tags( $new_instance[ $id ] ) : '';
        endforeach;      
        return $instance;
    }
} // Class wpb_widget ends here

// Register and load the widget
add_action( 'widgets_init', function(){
    register_widget( 'wpb_widget' );
} );